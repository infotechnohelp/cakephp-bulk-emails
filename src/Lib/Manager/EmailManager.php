<?php

namespace Infotechnohelp\CakePhp\BulkEmails\Lib\Manager;


use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Infotechnohelp\CakePhp\BulkEmails\Model\Entity\EmailSendingTask;
use TemplateManager\TemplateManager;

class EmailManager
{
    public function addSendingTask(
        string $to,
        string $subject,
        string $template = null,
        string $input = null,
        \DateTime $sendOn = null,
        string $from = null
    ): EmailSendingTask {
        /** @var \Infotechnohelp\CakePhp\BulkEmails\Model\Table\EmailSendingTasksTable $EmailSendingTasksTable */
        $EmailSendingTasksTable = TableRegistry::getTableLocator()
                                               ->get('Infotechnohelp/CakePhp/BulkEmails.EmailSendingTasks');

        return $EmailSendingTasksTable->saveOrFail($EmailSendingTasksTable->newEntity([
            '_from'    => $from,
            '_to'      => $to,
            'subject'  => $subject,
            'template' => $template,
            'input'    => $input,
            'send_on'  => $sendOn,
        ]));
    }

    public function executeSendingTask(EmailSendingTask $EmailSendingTask)
    {
        /** @var \Infotechnohelp\CakePhp\BulkEmails\Model\Table\EmailSendingsTable $EmailSendingsTable */
        $EmailSendingsTable = TableRegistry::getTableLocator()
                                           ->get('Infotechnohelp/CakePhp/BulkEmails.EmailSendings');

        // Email has already been sent
        if ($EmailSendingsTable->find()->where([
                'email_sending_task_id' => $EmailSendingTask->get('id'),
            ])->first() !== null
        ) {
            return false;
        }

        // Wait
        if ($EmailSendingsTable->find()->last() !== null) {
            /** @var \Cake\I18n\FrozenTime $sentOn */
            $sentOn = $EmailSendingsTable->find()->last()->get('created');

            $difference = strtotime((new \DateTime())->format('Y-m-d h:i:s')) -
                          strtotime($sentOn->format('Y-m-d h:i:s'));

            $sendingInterval = (new AutomationSettingsManager())->getSettingValue('sending_interval');

            $sleepingSeconds = (($sendingInterval - $difference) >= 0) ? $sendingInterval - $difference : 0;

            sleep($sleepingSeconds);
        }

        $from = $EmailSendingTask->get('_from');

        if ($from === null) {
            $from = (new AutomationSettingsManager())->getSettingValue('default_sender');
        }

        $to      = $EmailSendingTask->get('_to');
        $subject = $EmailSendingTask->get('subject');

        $input = json_decode($EmailSendingTask->get('input'), true);

        $input = ($input === null) ? [] : $input;

        $template = $EmailSendingTask->get('template') ?? (new AutomationSettingsManager())->getSettingValue('default_email_template');

        $text = (new TemplateManager($template))
            ->getText($input);


        if ( ! $this->isValidEmail($to)) {
            return $EmailSendingsTable->saveOrFail($EmailSendingsTable->newEntity([
                'email_sending_task_id' => $EmailSendingTask->get('id'),
                'success'               => false,
            ]));
        }

        $sendingResult = mail(
            $to,
            $subject,
            $text,
            "From: $from" . "\r\n" .
            'MIME-Version: 1.0' . "\r\n" .
            'Content-type: text/html; charset=UTF-8'
        );

        if ( ! $sendingResult) {
            Log::error("Email sending failure: $to EmailSendingTaskId: " . $EmailSendingTask->get('id'));

            return false;
        }

        return $EmailSendingsTable->saveOrFail($EmailSendingsTable->newEntity([
            'email_sending_task_id' => $EmailSendingTask->get('id'),
            'success'               => true,
        ]));
    }

    function isValidEmail($email)
    {
        $result = false;

        if ( ! preg_match('/^[_A-z0-9-]+((\.|\+)[_A-z0-9-]+)*@[A-z0-9-]+(\.[A-z0-9-]+)*(\.[A-z]{2,4})$/', $email)) {
            return $result;
        }

        list($name, $domain) = explode('@', $email);
        if ( ! checkdnsrr($domain, 'MX')) {
            return $result;
        }

        $max_conn_time = 30;
        $sock          = '';
        $port          = 25;
        $max_read_time = 5;
        $users         = $name;
        $hosts         = [];
        $mxweights     = [];
        getmxrr($domain, $hosts, $mxweights);
        $mxs = array_combine($hosts, $mxweights);
        asort($mxs, SORT_NUMERIC);
        $mxs[$domain] = 100;
        $timeout      = $max_conn_time / count($mxs);

        // ToDo
        return true;

        while (list($host) = each($mxs)) {
            if ($sock = fsockopen($host, $port, $errno, $errstr, (float)$timeout)) {
                stream_set_timeout($sock, $max_read_time);
                break;
            }
        }

        if ($sock) {
            $reply = fread($sock, 2082);
            preg_match('/^([0-9]{3}) /ims', $reply, $matches);
            $code = isset($matches[1]) ? $matches[1] : '';
            if ($code != '220') {
                return $result;
            }
            $msg = "HELO " . $domain;
            fwrite($sock, $msg . "\r\n");
            $reply = fread($sock, 2082);

            $msg = "MAIL FROM: <" . $name . '@' . $domain . ">";
            fwrite($sock, $msg . "\r\n");
            $reply = fread($sock, 2082);

            $msg = "RCPT TO: <" . $name . '@' . $domain . ">";
            fwrite($sock, $msg . "\r\n");
            $reply = fread($sock, 2082);

            preg_match('/^([0-9]{3}) /ims', $reply, $matches);
            $code = isset($matches[1]) ? $matches[1] : '';

            if ($code == '250') {
                $result = true;
            } else if ($code == '451' || $code == '452') {
                $result = true;
            } else {
                $result = false;
            }

            $msg = "quit";
            fwrite($sock, $msg . "\r\n");

            fclose($sock);
        }

        return $result;
    }
}