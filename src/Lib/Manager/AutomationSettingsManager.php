<?php

namespace Infotechnohelp\CakePhp\BulkEmails\Lib\Manager;

use Cake\ORM\TableRegistry;
use Infotechnohelp\CakePhp\BulkEmails\Model\Entity\BulkEmailAutomationSetting;

class AutomationSettingsManager
{
    private function findAutomationSetting(string $settingTitle): BulkEmailAutomationSetting
    {
        /** @var \Infotechnohelp\CakePhp\BulkEmails\Model\Table\BulkEmailAutomationSettingsTable $BulkEmailAutomationSettingsTable */
        $BulkEmailAutomationSettingsTable = TableRegistry::getTableLocator()
                                                         ->get('Infotechnohelp/CakePhp/BulkEmails.BulkEmailAutomationSettings');
        /** @var BulkEmailAutomationSetting $AutomationSetting */
        $AutomationSetting = $BulkEmailAutomationSettingsTable->findByTitle($settingTitle)->first();

        if ($AutomationSetting === null) {
            throw new \Exception(sprintf("Automation setting '%s' not found", $settingTitle));
        }

        return $AutomationSetting;
    }

    public function getSettingValue(string $settingTitle)
    {
        $AutomationSetting = $this->findAutomationSetting($settingTitle);

        $type = $AutomationSetting->get('_type');

        return $AutomationSetting->get('_' . $type);
    }

    public function setSettingValue(string $settingTitle, $value): BulkEmailAutomationSetting
    {
        $AutomationSetting = $this->findAutomationSetting($settingTitle);

        $type = $AutomationSetting->get('_type');

        $AutomationSetting->set('_' . $type, $value);

        /** @var \Infotechnohelp\CakePhp\BulkEmails\Model\Table\BulkEmailAutomationSettingsTable $BulkEmailAutomationSettingsTable */
        $BulkEmailAutomationSettingsTable = TableRegistry::getTableLocator()
                                                         ->get('Infotechnohelp/CakePhp/BulkEmails.BulkEmailAutomationSettings');
        /** @var \Infotechnohelp\CakePhp\BulkEmails\Model\Entity\BulkEmailAutomationSetting $result */
        $result = $BulkEmailAutomationSettingsTable->saveOrFail($AutomationSetting);

        return $result;
    }
}