<?php

namespace Infotechnohelp\CakePhp\BulkEmails\Lib;

use Cake\ORM\TableRegistry;
use Infotechnohelp\CakePhp\BulkEmails\Lib\Manager\AutomationSettingsManager;
use Infotechnohelp\CakePhp\BulkEmails\Lib\Manager\EmailManager;

class Automation
{
    public function run()
    {
        $start = time();

        $AutomationSettingsManager = new AutomationSettingsManager();

        $enabled = (new AutomationSettingsManager())->getSettingValue('enabled');

        if ( ! $enabled) {
            return false;
        }

        $processedItemCounter = 0;

        $automationExecutionTime = $AutomationSettingsManager->getSettingValue('automation_execution_time');
        $sendingInterval         = $AutomationSettingsManager->getSettingValue('sending_interval');
        $sendingDelay            = $AutomationSettingsManager->getSettingValue('sending_delay');

        /** @var \Infotechnohelp\CakePhp\BulkEmails\Model\Table\EmailSendingTasksTable $EmailSendingTasksTable */
        $EmailSendingTasksTable = TableRegistry::getTableLocator()
                                               ->get('Infotechnohelp/CakePhp/BulkEmails.EmailSendingTasks');

        $EmailManager = new EmailManager();

        foreach (
            $EmailSendingTasksTable->find()
                                   ->notMatching('EmailSendings')
                                   ->all() as $EmailSendingTask
        ) {

            $EmailManager->executeSendingTask($EmailSendingTask);
            $processedItemCounter++;

            $executionTime = time() - $start;

            if ($executionTime + $sendingInterval + $sendingDelay >= $automationExecutionTime) {
                return [
                    'execution_time'        => $executionTime,
                    'processed_item_amount' => $processedItemCounter,
                ];
            }
        }

        $executionTime = time() - $start;

        return [
            'execution_time'        => $executionTime,
            'processed_item_amount' => $processedItemCounter,
        ];
    }
}