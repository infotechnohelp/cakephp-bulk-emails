<?php

if (empty($result)) {
    echo 'Thanks, but You already had unsubscribed!';
}

if ($result) {
    echo 'Thank you! You have successfully unsubscribed!';
}

if ($result === false) {
    echo 'Sorry! Unsubscription went wrong. Please, contact ' . getenv('CONTACT_EMAIL');
}