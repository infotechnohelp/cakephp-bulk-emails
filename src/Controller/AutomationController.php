<?php

namespace Infotechnohelp\CakePhp\BulkEmails\Controller;

use Cake\Controller\Controller;
use Infotechnohelp\CakePhp\BulkEmails\Lib\Automation;

class AutomationController extends Controller
{
    public $autoRender = false;

    public function index()
    {
       (new Automation())->run();
    }
}