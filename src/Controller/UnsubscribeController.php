<?php

namespace Infotechnohelp\CakePhp\BulkEmails\Controller;

use Cake\Controller\Controller;
use Cake\ORM\TableRegistry;

class UnsubscribeController extends Controller
{
    public function index()
    {
        $result = null;

        $EmailSendingTasksTable = TableRegistry::getTableLocator()->get('Infotechnohelp/CakePhp/BulkEmails.EmailSendingTasks');

        $EmailSendingTask = $EmailSendingTasksTable->find()->where([
            'unsubscriptionToken' => $this->request->getQuery('token'),
            '_to'                 => $this->request->getQuery('email'),
        ])->first();

        if ($EmailSendingTask->get('unsubscribed') === true) {
            $this->set('result', null);

            return null;

        }

        if ( ! empty($EmailSendingTask)) {

            $EmailSendingTask->set('unsubscribed', true);

            $savingResult = $EmailSendingTasksTable->saveOrFail($EmailSendingTask);

            if (empty($result) && $savingResult !== false) {
                $result = true;
            }
        }

        $result = $result ?? false;

        $this->set('result', $result);
    }
}