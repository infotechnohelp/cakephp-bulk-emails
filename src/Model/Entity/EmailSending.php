<?php
namespace Infotechnohelp\CakePhp\BulkEmails\Model\Entity;

use Cake\ORM\Entity;

/**
 * EmailSending Entity
 *
 * @property int $id
 * @property int $email_sending_task_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \Infotechnohelp\CakePhp\BulkEmails\Model\Entity\EmailSendingTask $email_sending_task
 */
class EmailSending extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'email_sending_task_id' => true,
        'success' => true,
        'created' => true,
        'modified' => true,
        'email_sending_task' => true
    ];
}
