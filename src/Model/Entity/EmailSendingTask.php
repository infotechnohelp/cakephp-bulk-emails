<?php
namespace Infotechnohelp\CakePhp\BulkEmails\Model\Entity;

use Cake\ORM\Entity;

/**
 * EmailSendingTask Entity
 *
 * @property int $id
 * @property string $from
 * @property string $to
 * @property string $subject
 * @property string $template
 * @property string $input
 * @property \Cake\I18n\FrozenTime $send_on
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \Infotechnohelp\CakePhp\BulkEmails\Model\Entity\EmailSending[] $email_sendings
 */
class EmailSendingTask extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '_from' => true,
        '_to' => true,
        'subject' => true,
        'template' => true,
        'input' => true,
        'send_on' => true,
        'created' => true,
        'modified' => true,
        'email_sendings' => true
    ];
}
