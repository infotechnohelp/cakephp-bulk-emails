<?php
namespace Infotechnohelp\CakePhp\BulkEmails\Model\Entity;

use Cake\ORM\Entity;

/**
 * BulkEmailAutomationSetting Entity
 *
 * @property int $id
 * @property string $title
 * @property string $_type
 * @property bool $_boolean
 * @property int $_integer
 * @property string $_string
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class BulkEmailAutomationSetting extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        '_type' => true,
        '_boolean' => true,
        '_integer' => true,
        '_string' => true,
        'created' => true,
        'modified' => true
    ];
}
