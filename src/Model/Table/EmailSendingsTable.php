<?php
namespace Infotechnohelp\CakePhp\BulkEmails\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * EmailSendings Model
 *
 * @property \Infotechnohelp\CakePhp\BulkEmails\Model\Table\EmailSendingTasksTable|\Cake\ORM\Association\BelongsTo $EmailSendingTasks
 *
 * @method \Infotechnohelp\CakePhp\BulkEmails\Model\Entity\EmailSending get($primaryKey, $options = [])
 * @method \Infotechnohelp\CakePhp\BulkEmails\Model\Entity\EmailSending newEntity($data = null, array $options = [])
 * @method \Infotechnohelp\CakePhp\BulkEmails\Model\Entity\EmailSending[] newEntities(array $data, array $options = [])
 * @method \Infotechnohelp\CakePhp\BulkEmails\Model\Entity\EmailSending|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Infotechnohelp\CakePhp\BulkEmails\Model\Entity\EmailSending|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Infotechnohelp\CakePhp\BulkEmails\Model\Entity\EmailSending patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Infotechnohelp\CakePhp\BulkEmails\Model\Entity\EmailSending[] patchEntities($entities, array $data, array $options = [])
 * @method \Infotechnohelp\CakePhp\BulkEmails\Model\Entity\EmailSending findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class EmailSendingsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('email_sendings');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('EmailSendingTasks', [
            'foreignKey' => 'email_sending_task_id',
            'joinType' => 'INNER',
            'className' => 'Infotechnohelp/CakePhp/BulkEmails.EmailSendingTasks'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['email_sending_task_id'], 'EmailSendingTasks'));

        return $rules;
    }
}
