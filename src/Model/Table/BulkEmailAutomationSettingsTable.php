<?php
namespace Infotechnohelp\CakePhp\BulkEmails\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BulkEmailAutomationSettings Model
 *
 * @method Query findByTitle(string $settingTitle)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class BulkEmailAutomationSettingsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('bulk_email_automation_settings');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('title')
            ->maxLength('title', 191)
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->scalar('_type')
            ->maxLength('_type', 191)
            ->requirePresence('_type', 'create')
            ->notEmpty('_type');

        $validator
            ->boolean('_boolean')
            ->allowEmpty('_boolean');

        $validator
            ->integer('_integer')
            ->allowEmpty('_integer');

        $validator
            ->scalar('_string')
            ->maxLength('_string', 191)
            ->allowEmpty('_string');

        return $validator;
    }
}
