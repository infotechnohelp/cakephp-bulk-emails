<?php
namespace Infotechnohelp\CakePhp\BulkEmails\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * EmailSendingTasks Model
 *
 * @property \Infotechnohelp\CakePhp\BulkEmails\Model\Table\EmailSendingsTable|\Cake\ORM\Association\HasMany $EmailSendings
 *
 * @method \Infotechnohelp\CakePhp\BulkEmails\Model\Entity\EmailSendingTask get($primaryKey, $options = [])
 * @method \Infotechnohelp\CakePhp\BulkEmails\Model\Entity\EmailSendingTask newEntity($data = null, array $options = [])
 * @method \Infotechnohelp\CakePhp\BulkEmails\Model\Entity\EmailSendingTask[] newEntities(array $data, array $options = [])
 * @method \Infotechnohelp\CakePhp\BulkEmails\Model\Entity\EmailSendingTask|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Infotechnohelp\CakePhp\BulkEmails\Model\Entity\EmailSendingTask|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Infotechnohelp\CakePhp\BulkEmails\Model\Entity\EmailSendingTask patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Infotechnohelp\CakePhp\BulkEmails\Model\Entity\EmailSendingTask[] patchEntities($entities, array $data, array $options = [])
 * @method \Infotechnohelp\CakePhp\BulkEmails\Model\Entity\EmailSendingTask findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class EmailSendingTasksTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('email_sending_tasks');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('EmailSendings', [
            'foreignKey' => 'email_sending_task_id',
            'className' => 'Infotechnohelp/CakePhp/BulkEmails.EmailSendings'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('_from')
            ->maxLength('_from', 191)
            ->allowEmpty('_from');

        $validator
            ->scalar('_to')
            ->maxLength('_to', 191)
            ->requirePresence('_to', 'create')
            ->notEmpty('_to');

        $validator
            ->scalar('subject')
            ->maxLength('subject', 191)
            ->requirePresence('subject', 'create')
            ->notEmpty('subject');

        $validator
            ->scalar('template')
            ->allowEmpty('template');

        $validator
            ->scalar('input')
            ->allowEmpty('input');

        $validator
            ->dateTime('send_on')
            ->allowEmpty('send_on');

        return $validator;
    }
}
