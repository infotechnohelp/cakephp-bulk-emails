### Load plugin
Add this line to `APP/config/bootstrap.php`

`Plugin::load('Infotechnohelp/CakePhp/BulkEmails');`

### Migrate tables into APP
CakePHP 3+ APP root

`$ bin/cake migrations migrate --plugin Infotechnohelp/CakePhp/BulkEmails`

`$ bin/cake migrations seed --plugin Infotechnohelp/CakePhp/BulkEmails`