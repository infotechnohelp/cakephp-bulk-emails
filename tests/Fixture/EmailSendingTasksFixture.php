<?php

namespace Infotechnohelp\CakePhp\BulkEmails\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * EmailSendingTasksFixture
 *
 */
class EmailSendingTasksFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id'           => [
            'type'          => 'integer',
            'length'        => 11,
            'unsigned'      => false,
            'null'          => false,
            'default'       => null,
            'comment'       => '',
            'autoIncrement' => true,
            'precision'     => null,
        ],
        '_from'         => [
            'type'      => 'string',
            'length'    => 191,
            'null'      => true,
            'default'   => null,
            'collate'   => 'utf8_unicode_ci',
            'comment'   => '',
            'precision' => null,
            'fixed'     => null,
        ],
        '_to'           => [
            'type'      => 'string',
            'length'    => 191,
            'null'      => false,
            'default'   => null,
            'collate'   => 'utf8_unicode_ci',
            'comment'   => '',
            'precision' => null,
            'fixed'     => null,
        ],
        'subject'      => [
            'type'      => 'string',
            'length'    => 191,
            'null'      => false,
            'default'   => null,
            'collate'   => 'utf8_unicode_ci',
            'comment'   => '',
            'precision' => null,
            'fixed'     => null,
        ],
        'template'     => [
            'type'      => 'text',
            'length'    => null,
            'null'      => true,
            'default'   => null,
            'collate'   => 'utf8_unicode_ci',
            'comment'   => '',
            'precision' => null,
        ],
        'input'        => [
            'type'      => 'text',
            'length'    => null,
            'null'      => true,
            'default'   => null,
            'collate'   => 'utf8_unicode_ci',
            'comment'   => '',
            'precision' => null,
        ],
        'send_on'      => [
            'type'      => 'datetime',
            'length'    => null,
            'null'      => true,
            'default'   => null,
            'comment'   => '',
            'precision' => null,
        ],
        'created'      => [
            'type'      => 'datetime',
            'length'    => null,
            'null'      => false,
            'default'   => null,
            'comment'   => '',
            'precision' => null,
        ],
        'modified'     => [
            'type'      => 'datetime',
            'length'    => null,
            'null'      => false,
            'default'   => null,
            'comment'   => '',
            'precision' => null,
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options'     => [
            'engine'    => 'InnoDB',
            'collation' => 'utf8_unicode_ci',
        ],
    ];
    // @codingStandardsIgnoreEnd
}
