<?php

namespace Infotechnohelp\CakePhp\BulkEmails\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * BulkEmailAutomationSettingsFixture
 *
 */
class BulkEmailAutomationSettingsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id'           => [
            'type'          => 'integer',
            'length'        => 11,
            'unsigned'      => false,
            'null'          => false,
            'default'       => null,
            'comment'       => '',
            'autoIncrement' => true,
            'precision'     => null,
        ],
        'title'        => [
            'type'      => 'string',
            'length'    => 191,
            'null'      => false,
            'default'   => null,
            'collate'   => 'utf8_unicode_ci',
            'comment'   => '',
            'precision' => null,
            'fixed'     => null,
        ],
        '_type'        => [
            'type'      => 'string',
            'length'    => 191,
            'null'      => false,
            'default'   => null,
            'collate'   => 'utf8_unicode_ci',
            'comment'   => '',
            'precision' => null,
            'fixed'     => null,
        ],
        '_boolean'     => [
            'type'      => 'boolean',
            'length'    => null,
            'null'      => true,
            'default'   => null,
            'comment'   => '',
            'precision' => null,
        ],
        '_integer'     => [
            'type'          => 'integer',
            'length'        => 11,
            'unsigned'      => false,
            'null'          => true,
            'default'       => null,
            'comment'       => '',
            'precision'     => null,
            'autoIncrement' => null,
        ],
        '_string'      => [
            'type'      => 'string',
            'length'    => 191,
            'null'      => true,
            'default'   => null,
            'collate'   => 'utf8_unicode_ci',
            'comment'   => '',
            'precision' => null,
            'fixed'     => null,
        ],
        '_text'        => [
            'type'      => 'text',
            'null'      => true,
            'default'   => null,
            'collate'   => 'utf8_unicode_ci',
            'comment'   => '',
            'precision' => null,
            'fixed'     => null,
        ],
        'created'      => [
            'type'      => 'datetime',
            'length'    => null,
            'null'      => false,
            'default'   => null,
            'comment'   => '',
            'precision' => null,
        ],
        'modified'     => [
            'type'      => 'datetime',
            'length'    => null,
            'null'      => false,
            'default'   => null,
            'comment'   => '',
            'precision' => null,
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options'     => [
            'engine'    => 'InnoDB',
            'collation' => 'utf8_unicode_ci',
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $emailTemplate = '<img src="https://nikolajev.ee/LibraLogo.PNG"/> <br/>' .
                         'Lugupeetud {{company_name}} esindaja, <br/><br/>' .
                         'Libra Tõlkebürool on hea meel teatada, et alates tänasest kuni 1.08.2018 me pakume 10% allahindlust kõigilt meie tõlketeenustelt! ' .
                         'Pakkumise aktiveerimiseks palun kasutage boonuskoodi LIBRA10 enne tellimuse esitamist. <br/><br/>' .
                         'Meie tõlkebüroo eesmärgiks on pakkuda alati parimat kvaliteeti ja täpsust kõigis oma tegemistes. ' .
                         'Me oleme huvitatud koostööst Teie firmaga ning pakume väga hea hinnakirja kirjalikule tõlkele: <br/>' .
                         '<b>Inglise-eesti-inglise al. 12.00 €<br/>' .
                         'Vene-eesti-vene al. 12.00 €<br/>' .
                         'Soome-eesti-soome al. 13.00 €<br/>' .
                         'Saksa-eesti-saksa al. 13.00 €<br/>' .
                         'Läti-eesti-läti al. 15.00 €</b> <br/><br/>' .
                         'Meie käest saab alati küsida erinevate keelesuundade hinnapakkumisi, isegi kuid neid kirjas ei ole. Tutvuge ka meie kodulehega:<br/>' .
                         '<a href="http://www.libra.ee">http://www.libra.ee/</a> <br/>' .
                         'Loodame edasist edukat suhtlust ja koostööd Teie firmaga. <br/> <br/>' .
                         'Lugupidamisega,<br/>' .
                         'Libra Tõlkebüroo meeskond<br/><br/>' .

                         '<b><a href="http://www.libra.ee">Libra Tõlkebüroo</a></b> <br/>' .
                         'Tel: (+372) <b>6 444 333</b> <br/>' .
                         'Peterburi tee 47, Tallinn, Estonia, 11415';

        $this->records = [
            [
                'id'       => 1,
                'title'    => 'enabled',
                '_type'    => 'boolean',
                '_boolean' => true,
                '_integer' => null,
                '_string'  => null,
                'created'  => '2018-07-22 10:33:29',
                'modified' => '2018-07-22 10:33:29',
            ],

            [
                'id'       => 2,
                'title'    => 'default_sender',
                '_type'    => 'string',
                '_boolean' => null,
                '_integer' => null,
                '_string'  => 'philipp.nikolajev@gmail.com',
                'created'  => '2018-07-22 10:33:29',
                'modified' => '2018-07-22 10:33:29',
            ],

            [
                'id'       => 3,
                'title'    => 'sending_interval',
                '_type'    => 'integer',
                '_boolean' => null,
                '_integer' => 7,
                '_string'  => null,
                'created'  => '2018-07-22 10:33:29',
                'modified' => '2018-07-22 10:33:29',
            ],

            [
                'id'       => 4,
                'title'    => 'sending_delay',
                '_type'    => 'integer',
                '_boolean' => null,
                '_integer' => 5,
                '_string'  => null,
                'created'  => '2018-07-22 10:33:29',
                'modified' => '2018-07-22 10:33:29',
            ],

            [
                'id'       => 5,
                'title'    => 'automation_execution_time',
                '_type'    => 'integer',
                '_boolean' => null,
                '_integer' => 33,
                '_string'  => null,
                'created'  => '2018-07-22 10:33:29',
                'modified' => '2018-07-22 10:33:29',
            ],

            [
                'id'       => 6,
                'title'    => 'default_email_template',
                '_type'    => 'text',
                '_boolean' => null,
                '_integer' => null,
                '_string'  => null,
                '_text'    => $emailTemplate,
                'created'  => '2018-07-22 10:33:29',
                'modified' => '2018-07-22 10:33:29',
            ],
        ];
        parent::init();
    }
}
