<?php

namespace Infotechnohelp\CakePhp\BulkEmails\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * EmailSendingsFixture
 *
 */
class EmailSendingsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id'                    => [
            'type'          => 'integer',
            'length'        => 11,
            'unsigned'      => false,
            'null'          => false,
            'default'       => null,
            'comment'       => '',
            'autoIncrement' => true,
            'precision'     => null,
        ],
        'email_sending_task_id' => ['type'          => 'integer',
                                    'length'        => 11,
                                    'unsigned'      => false,
                                    'null'          => false,
                                    'default'       => null,
                                    'comment'       => '',
                                    'precision'     => null,
                                    'autoIncrement' => null,
        ],
        'success'     => [
            'type'      => 'boolean',
            'length'    => null,
            'null'      => false,
            'default'   => null,
            'comment'   => '',
            'precision' => null,
        ],
        'created'               => ['type'      => 'datetime',
                                    'length'    => null,
                                    'null'      => false,
                                    'default'   => null,
                                    'comment'   => '',
                                    'precision' => null,
        ],
        'modified'              => ['type'      => 'datetime',
                                    'length'    => null,
                                    'null'      => false,
                                    'default'   => null,
                                    'comment'   => '',
                                    'precision' => null,
        ],
        '_constraints'          => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options'              => [
            'engine'    => 'InnoDB',
            'collation' => 'utf8_unicode_ci',
        ],
    ];
    // @codingStandardsIgnoreEnd
}
