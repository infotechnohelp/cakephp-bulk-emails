<?php
namespace Infotechnohelp\CakePhp\BulkEmails\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Infotechnohelp\CakePhp\BulkEmails\Model\Table\EmailSendingsTable;

/**
 * Infotechnohelp\CakePhp\BulkEmails\Model\Table\EmailSendingsTable Test Case
 */
class EmailSendingsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Infotechnohelp\CakePhp\BulkEmails\Model\Table\EmailSendingsTable
     */
    public $EmailSendings;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.infotechnohelp/cake_php/bulk_emails.email_sendings',
        'plugin.infotechnohelp/cake_php/bulk_emails.email_sending_tasks'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('EmailSendings') ? [] : ['className' => EmailSendingsTable::class];
        $this->EmailSendings = TableRegistry::getTableLocator()->get('EmailSendings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->EmailSendings);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
