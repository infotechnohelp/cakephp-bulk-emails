<?php
namespace Infotechnohelp\CakePhp\BulkEmails\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Infotechnohelp\CakePhp\BulkEmails\Model\Table\BulkEmailAutomationSettingsTable;

/**
 * Infotechnohelp\CakePhp\BulkEmails\Model\Table\BulkEmailAutomationSettingsTable Test Case
 */
class BulkEmailAutomationSettingsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Infotechnohelp\CakePhp\BulkEmails\Model\Table\BulkEmailAutomationSettingsTable
     */
    public $BulkEmailAutomationSettings;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.infotechnohelp/cake_php/bulk_emails.bulk_email_automation_settings'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('BulkEmailAutomationSettings') ? [] : ['className' => BulkEmailAutomationSettingsTable::class];
        $this->BulkEmailAutomationSettings = TableRegistry::getTableLocator()->get('BulkEmailAutomationSettings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BulkEmailAutomationSettings);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
