<?php
namespace Infotechnohelp\CakePhp\BulkEmails\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Infotechnohelp\CakePhp\BulkEmails\Model\Table\EmailSendingTasksTable;

/**
 * Infotechnohelp\CakePhp\BulkEmails\Model\Table\EmailSendingTasksTable Test Case
 */
class EmailSendingTasksTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Infotechnohelp\CakePhp\BulkEmails\Model\Table\EmailSendingTasksTable
     */
    public $EmailSendingTasks;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.infotechnohelp/cake_php/bulk_emails.email_sending_tasks',
        'plugin.infotechnohelp/cake_php/bulk_emails.email_sendings'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('EmailSendingTasks') ? [] : ['className' => EmailSendingTasksTable::class];
        $this->EmailSendingTasks = TableRegistry::getTableLocator()->get('EmailSendingTasks', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->EmailSendingTasks);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
