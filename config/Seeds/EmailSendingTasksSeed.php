<?php

use Migrations\AbstractSeed;

/**
 * Class AutomationSettingsSeed
 */
class EmailSendingTasksSeed extends AbstractSeed
{
    public function run()
    {
        $bulkEmails = json_decode(
            file_get_contents('vendor/infotechnohelp/cakephp-bulk-emails/config/Seeds/bulk-emails.json'),
            true);

        $data = [];

        foreach ($bulkEmails as $bulkEmail) {

            $token = bin2hex(random_bytes(16));

            $data[] = [
                '_to'                 => $bulkEmail[0],
                'subject'             => 'Libra Tõlkebüroo pakkumised',
                'template'            => null,
                'input'               => '{"company_name":"' . $bulkEmail[1] . '", "unsubscriptionToken":"' .
                                         $token . '", "email":"' .
                                         $bulkEmail[0] . '"}',
                'unsubscriptionToken' => $token,
            ];
        }


        $table = $this->table('email_sending_tasks');
        $table->insert($data)->save();
    }
}
