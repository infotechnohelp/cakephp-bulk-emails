<?php

use Migrations\AbstractSeed;

/**
 * Class AutomationSettingsSeed
 */
class BulkEmailAutomationSettingsSeed extends AbstractSeed
{
    public function run()
    {
        $emailTemplate = 'Lugupeetud {{company_name}} esindaja, <br/><br/><br/>' .
                         'Libra 
Tõlkebürool on hea meel teatada, et alates tänasest kuni 1.09.2018 me pakume 10% allahindlust kõigilt meie 
tõlketeenustelt! ' .
                         'Pakkumise aktiveerimiseks palun kasutage boonuskoodi LIBRA10 enne tellimuse esitamist. 
<br/><br/>' .
                         'Meie tõlkebüroo eesmärgiks on pakkuda alati parimat kvaliteeti ja täpsust kõigis oma tegemistes. ' .
                         'Me oleme 
huvitatud koostööst Teie firmaga ning pakume väga hea hinnakirja kirjalikule tõlkele: <br/><br/><br/>' .
                         '<b>Inglise-eesti-inglise al. 12.00 €<br/><br/>' .
                         'Vene-eesti-vene al. 12.00 €<br/><br/>' .
                         'Soome-eesti-soome al. 13.00 €<br/><br/>' .
                         'Saksa-eesti-saksa al. 13.00 €<br/><br/>' .
                         'Läti-eesti-läti al. 15.00 €</b> <br/><br/><br/>' .
                         'Meie käest saab alati küsida erinevate keelesuundade hinnapakkumisi, isegi kui neid kirjas ei ole. Tutvuge ka meie kodulehega:<br/><br/>' .
                         '<a href="http://www.libra.ee">http://www.libra.ee/</a> <br/><br/>' .
                         'Loodame edasist edukat suhtlust ja koostööd Teie firmaga. <br/> <br/><br/>' .
                         'Lugupidamisega,<br/>' .
                         'Libra Tõlkebüroo meeskond<br/><br/>' .

                         '<b><a href="' . getenv('MAILER_URL') . '/bulk-emails/unsubscribe?' .
                         'token={{unsubscriptionToken}}&email={{email}}">Unsubscribe</a> </b> <br/> <br/>' .

                         '<b><a href="http://www.libra.ee">Libra Tõlkebüroo</a></b> <br/>' .
                         'Tel: (+372) <b>6 444 333</b> <br/>' .
                         '<a href="https://www.google.com/maps/place/Peterburi+tee+47,+11415+Tallinn/@59.4273871,24.8067602,17z/data=!3m1!4b1!4m5!3m4!1s0x4692eca914d1fcbd:0x754ddac2802ae834!8m2!3d59.4273871!4d24.8089489">
Peterburi tee 47, Tallinn, Estonia, 11415</a> <br/>' .
                         '<img src="https://nikolajev.ee/LibraLogo.PNG"/>';

        $data = [
            [
                'title'    => 'enabled',
                '_type'    => 'boolean',
                '_boolean' => true,
                'created'  => (new \DateTime())->format('Y-m-d'),
                'modified' => (new \DateTime())->format('Y-m-d'),
            ],
            [
                'title'    => 'default_sender',
                '_type'    => 'string',
                '_string'  => 'info@libra.ee',
                'created'  => (new \DateTime())->format('Y-m-d'),
                'modified' => (new \DateTime())->format('Y-m-d'),
            ],
            [
                'title'    => 'sending_interval',
                '_type'    => 'integer',
                '_integer' => 7,
                'created'  => (new \DateTime())->format('Y-m-d'),
                'modified' => (new \DateTime())->format('Y-m-d'),
            ],
            [
                'title'    => 'sending_delay',
                '_type'    => 'integer',
                '_integer' => 5,
                'created'  => (new \DateTime())->format('Y-m-d'),
                'modified' => (new \DateTime())->format('Y-m-d'),
            ],
            [
                'title'    => 'automation_execution_time',
                '_type'    => 'integer',
                '_integer' => 300,
                'created'  => (new \DateTime())->format('Y-m-d'),
                'modified' => (new \DateTime())->format('Y-m-d'),
            ],
            [
                'title'    => 'default_email_template',
                '_type'    => 'text',
                '_text'    => $emailTemplate,
                'created'  => (new \DateTime())->format('Y-m-d'),
                'modified' => (new \DateTime())->format('Y-m-d'),
            ],
        ];

        $table = $this->table('bulk_email_automation_settings');
        $table->insert($data)->save();
    }
}