<?php
use Migrations\AbstractMigration;

class CreateEmailSendingTasks extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('email_sending_tasks');
        $table->addColumn('_from', 'string', [
            'default' => null,
            'limit' => 191,
            'null' => true,
        ]);
        $table->addColumn('_to', 'string', [
            'default' => null,
            'limit' => 191,
            'null' => false,
        ]);
        $table->addColumn('subject', 'string', [
            'default' => null,
            'limit' => 191,
            'null' => false,
        ]);
        $table->addColumn('template', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('input', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('send_on', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('unsubscribed', 'boolean', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('unsubscriptionToken', 'string', [
            'default' => null,
            'limit' => 191,
            'null' => false,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->create();
    }
}
